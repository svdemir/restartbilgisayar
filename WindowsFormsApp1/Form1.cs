﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            CheckForIllegalCrossThreadCalls = false;
            InitializeComponent();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        int sayac = 15;

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (sayac > 0)
            {
                try
                {
                    progressBar1.Value += 100 / 13;
                }
                catch (Exception)
                {
                }
                sayac = sayac - 1;
                label2.Text = sayac.ToString();
                label2.Refresh();
                if (sayac == 0)
                {
                    System.Diagnostics.Process.Start("ShutDown", "/r -f -t 1");
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AnaForm fm = new AnaForm();
            fm.timer1.Enabled = true;
            fm.timer1.Interval = 900000;
            this.Dispose();
        }
    }
}
